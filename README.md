# Yet Another Messanger - YAM

A Node ExpressJS application for instant messanging, SMS and E-mail.

### External Dependencies

- Postgresql

    We assume that you have `Postgresql` installed and running on your system.
    Make sure you have a database created with following configurations.
    
    Delvelopment Configurations:
    ```
    database : yam
    user     : yam
    password : mini-project
    ```

### Starting Server

- `npm install` to install the project dependencies.

- `npm start`
