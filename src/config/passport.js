/*
 * Import Dependencies
 */
const passport = require('passport');
const db = require('../models/db');
const LocalStrategy = require('passport-local').Strategy;

/*
 * passport configuration for sessions
 */
passport.serializeUser(function(user, done) {
    done(null, user)
});

passport.deserializeUser(function(user, done) {
    done(null, user)
});

/*
 * local authentication strategy
 */

passport.use('local-auth', new LocalStrategy({
        usernameField: 'user',
        passwordField : 'pwd',
        passReqToCallback: true
    },
    function(req, user, pwd, done) {
        db.User.findOne({where : {username : user}})
            .then(function(user){
                if(!user) {
                    console.log('User does not exist');
                    return done(null, false);
                }
                else{
                    pwd = require('crypto')
                          .createHash('sha1')
                          .update(pwd)
                          .digest('base64');
                    if(user.password !== pwd) {
                        console.log('Incorrect password');
                        return done(null, false);
                    }
                }
                return done(null, user.getAPIdata())
            });
    }
));


/*
 * export passport
 */
module.exports = passport;
