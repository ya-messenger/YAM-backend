const path = require('path');

exports.YAM_BASE_URL = process.env.YAM_BASE_URL || 'http://localhost:3000'
exports.IMAGES_DIR = process.env.IMAGES_DIR || path.resolve(__dirname + '/../../uploads/profile')
