exports.localRegistration = function(req, res) {
    let User = require('../../models/db').User;
    let username = req.body.user;
    let phone = req.body.phone;
    let email = req.body.email;
    let pwd = require('crypto')
        .createHash('sha1')
        .update(req.body.pwd)
        .digest('base64');
    let data = {};
    User.create({
            username: username,
            password: pwd,
            phone: phone,
            email: email
        })
        .then(function(user) {
            console.log('New user registered');
            data.id = user.id;
            data.user = user.username;
            res.json(data);
        })
        .catch(function(err) {
            console.log('User already exists');
            data.message = 'Username not available';
            console.log(err);
            res.status(403);
            res.json(data);
        });
};
