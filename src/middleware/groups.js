/*-------get a list of all the groups the logged in user ------*/
exports.getAuthUserGroups = function(req, res) {
    const db = require('../models/db');
    db.groupMember.findAll({
        where: {
            user_id: req.user.id
        }
    }).then(function(groups) {
        res.json(groups)
    })
};

/*--------create new group for the logged in user-------------*/
exports.createNewGroup = function(req, res){
    const db = require('../models/db');
    db.Group.create({
        name: req.body.groupName,
        purpose: req.body.purpose,
        header: req.body.header,
        creator_id: req.user.id
    }).then(function(group){
        db.groupMember.create({
            user_id: req.user.id,
            group_id: group.id,
            permissions: 'owner'
        })
        res.json({
            id: group.id,
            group_name: group.name
        })
    })
};

/*--------------------find group by id-------------------------*/
exports.getGroupById = function(req, res){
    const db = require('../models/db');
    db.Group.findById(req.params.group_id)
        .then(function(group){
            if(group){
                res.json(group.getAPIData());
            }
            else{
                res.status(404);
                res.json({message: "now group with id " + req.params.group_id +" exists"});
            }
        });
};

/*-------------------------------------------------------------*/
