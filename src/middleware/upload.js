exports.uploadProfile = function(req, res) {
    const db = require('../models/db');
    db.User.update({
        avatar: req.file.filename
    }, {
        where: {
            id: req.user.id
        }
    }).then(function(result) {
        res.send(req.file.filename)
    })
};

exports.uploadGroupAvatar= function(req, res) {
    const db = require('../models/db');
    db.groupMember.find({
        attributes: ['permissions'],
        where: {
            $and: {
                user_id: req.user.id,
                group_id: req.params.id,
            }
        }
    }).then(function(result) {
        if(result){
            if(['owner', 'master'].indexOf(result.permissions) !== -1) {
                db.Group.update({
                    avatar: req.file.filename
                }, {
                    where: {
                        id: req.params.id
                    }
                }).then(function(result) {
                    res.send(req.file.filename)
                })
            }
            else {
                res.status(403);
                res.json({"message": "Unautorized to add/change group avatar"})
            }
        }
        else {
            res.status(404);
            res.json({"message": "No group with this id exists"});
        }
    }).catch(function(err) {
        res.sendStatus(500);
    })
};
