exports.localAuth = function(req, res) {
    var data = {
        'id': req.user.id,
        'user': req.user.username
    };
    res.json(data)
};

exports.getLocalAuth = function(req, res) {
    let data = {};
    if(req.user){
        data.id = req.user.id;
        data.user = req.user.username;
        res.status(200);
    }
    else{
        data.message = "Unauthorized";
        res.status(401);
    }
    res.json(data);
};
