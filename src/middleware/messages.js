exports.getMessages = function(req, res){
    const db = require('../models/db');
    let data = {};
    db.Message.findAll({
        where: {
            read: false
        }
    }).then(function(messages){
        if(messages){
            data.error = false;
            console.log(messages.length)
            data.items = messages.length;
            data.messages = messages;
            res.json(data)
        }
        else{
            res.json({error: false})
        }
    });
};


exports.sendMessage = function(req, res){
    const db = require('../models/db');
    let data = {};
    db.Message.create({
        text: req.body.text,
        read: false
    })
    .then(function(message) {
        console.log('Message Saved');
        data.error = false;
        data.message = 'Message successfully sent';
        res.json(data);
    })
    .catch(function(err){
        console.log('Error saving message');
        data.error = true;
        data.message = 'Some error occured';
        res.json(data);
    });
};

/*=========== middleware to get single message by ID ===============*/

exports.getMessageById = function(req, res){
    const db = require('../models/db');
    let data = {};
    db.Message.findOne({
        where: {
            id: req.params.id
        }
    }).then(function(message){
        if(message){
            data = message;
            res.json(data);
        }
        else{
            data.error = true;
            data.message = "No message found with id " + req.params.id;
            res.json(data);
        }
    })
    .catch(function(err){
        data.error = true;
        data.message = 'Database error occured';
        res.json(data);
    });
};
