/*--------- Response with the details of user passed in parameters---------*/
exports.getUserbyId = function(req, res) {
    const db = require('../models/db');
    let userId = req.params.id;
    let data = {
        error: 'true',
        message: "No user with id '" + userId + "' exists"
    };

    // Find user with username
    if(isNaN(parseInt(userId))){
        db.User.findOne({
            where: {
                username: userId
            }
        }).then(function(userData) {
            if(userData) {
                res.json(userData.getAPIdata());
            }
            else {
                res.status(404);
                res.json(data)
            }
        })
    }

    // Find user with id
    else {
        db.User.findById(userId
        ).then(function(userData) {
            if(userData) {
                res.json(userData.getAPIdata())
            }
            else{
                res.status(404);
                res.json(data);
            }
        });
    }
}; 

/*-------------- Response with the data of logged in user -------------------------*/

exports.getAuthenticatedUser = function(req, res) {
    const db = require('../models/db');
    let authUserId = req.user.id;
    db.User.findById(authUserId).then(function(userData) {
        res.json(userData.getAPIdata());
    });
};
/*---------------------------------------------------------------------------------*/
