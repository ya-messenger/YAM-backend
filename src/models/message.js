module.exports = function(db) {
    const messageSchema = require('./schemas/message_schema');
    const Message = db.define('messages',
            messageSchema.messageSchema,
            {underscored: true});
    return Message;
};
