module.exports = function(db){
    const groupSchema = require('./schemas/group_schema');
    const env = require('../config/env');
    let Group = db.define('groups',
            groupSchema.groupSchema,
            {
                underscored: true,
                getterMethods: {
                    avatar_url: function(){
                        if(this.avatar){
                            let avatar_url = env.YAM_BASE_URL + '/images/' + this.avatar;
                            return avatar_url;
                        }
                        return env.YAM_BASE_URL + '/images/group-default-001';
                    }
                }
            });
    Group.prototype.getAPIData = function(){
        return {
            id: this.id,
            name: this.name,
            header: this.header || "",
            purpose: this.purpose || "",
            avatar_url: this.avatar_url,
            creator_id: this.creator_id,
            created_at: this.created_at,
            updated_at: this.updated_at
        }
    }

    return Group;
};
