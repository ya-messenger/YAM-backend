module.exports = function(db) {
    const userSchema = require('./schemas/user_schema');
    const env = require('../config/env');
    const User = db.define('user',
            userSchema.userSchema,
            {
                underscored: true,
                getterMethods: {
                    fullname: function(){
                        let name = '';
                        if(this.first_name) {
                            name = name + this.first_name;
                        }
                        name = name + ' ';
                        if(this.last_name) {
                            name = name + this.last_name;
                        }
                        return name.trim();
                    },
                    avatar_url: function(){
                        if(this.avatar){
                            let avatar_url = env.YAM_BASE_URL + '/images/' + this.avatar;
                            return avatar_url;
                        }
                        return env.YAM_BASE_URL + '/images/default-001';
                    }
                }
            });

    User.prototype.getAPIdata = function() {
        return {
            id: this.id,
            username: this.username,
            email: this.email ? this.email : "",
            name: this.fullname,
            phone: this.phone,
            bio: this.bio ? this.bio : "",
            avatar_url: this.avatar_url,
            blood_type: this.blood_type ? this.bio : "",
            role: this.role ? this.role : "",
            updated_at: this.updated_at,
            created_at: this.created_at
        }
    };

    return User;
};
