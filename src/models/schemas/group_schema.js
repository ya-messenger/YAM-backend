const Sequelize = require('sequelize');

exports.groupSchema = {
    "name": {
        "type": Sequelize.STRING,
        "notEmpty": true,
        "unique": true
    },
    "purpose":{
        "type": Sequelize.STRING(1000)
    },
    "header": {
        "type": Sequelize.STRING
    },
    "avatar": {
        "type": Sequelize.STRING
    },
    "creator_id": {
        "type": Sequelize.INTEGER,
        references: {
            model: "users",
            key: "id"
        }
    }
}
