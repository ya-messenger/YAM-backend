const Sequelize = require('sequelize');

exports.messageSchema = {
    "sender_id": {
        "type": Sequelize.INTEGER,
        references: {
            model: "users",
            key: "id"
        }
    },
    "group_id": {
        "type": Sequelize.INTEGER,
        references:{
            model: "groups",
            key: "id"
        }
    },
    "parent_id": {
        "type": Sequelize.INTEGER,
        references: {
            model: "messages",
            key: "id"
        }
    },
    "message": {
        "type": Sequelize.STRING(1500),
        "notEmpty": true
    },
    "read": {
        "type": Sequelize.BOOLEAN,
        "notEmpty": true
    }
};
