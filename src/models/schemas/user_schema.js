const Sequelize = require('sequelize');

exports.userSchema = {
    "id": {
        "type": Sequelize.INTEGER,
        "autoIncrement": true,
        "primaryKey": true
    },
    "username": {
        "type": Sequelize.STRING,
        "unique": true,
        "notEmpty": true
    },
    "phone": {
        "type": Sequelize.STRING,
        "unique": true,
        "notEmpty": true
    },
    "password": {
        "type": Sequelize.STRING,
        "notEmpty": true
    },
    "email": {
        "type": Sequelize.STRING,
        validate : {
            isEmail: true
        }
    },
    "first_name": {
        "type": Sequelize.STRING
    },
    "last_name": {
        "type": Sequelize.STRING
    },
    "bio": {
        "type": Sequelize.STRING(100)
    },
    "blood_type": {
        "type": Sequelize.ENUM("A+", "B+", "AB+", "O+", "A-", "B-", "AB-", "O-")
    },
    "avatar": {
        "type": Sequelize.STRING
    },
    "role": {
        "type": Sequelize.STRING
    }
}
