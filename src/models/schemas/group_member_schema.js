const Sequelize = require('sequelize');

exports.groupMemberSchema = {
    "user_id": {
        "type": Sequelize.INTEGER,
        references: {
            model: "users",
            key: "id"
        }
    },
    "group_id": {
        "type": Sequelize.INTEGER,
        references: {
            model: "groups",
            key: "id"
        }
    },
    "permissions": {
        "type": Sequelize.ENUM('owner', 'master', 'member')
    }
};
