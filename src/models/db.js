const Sequelize = require('sequelize');
const env = process.env.YAM_ENV || 'dev';
const config = require('../config/database.json')[env];

var db = {};

const sequelize = new Sequelize(config.database,
    config.username,
    config.password,
    config);

db.User = require('./user')(sequelize);
db.Message = require('./message')(sequelize);
db.Group = require('./group')(sequelize);
db.groupMember = require('./group_member')(sequelize);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
