module.exports = function(db) {
    const groupMemberSchema = require('./schemas/group_member_schema');
    const groupMember = db.define('group_member',
            groupMemberSchema.groupMemberSchema,
            {underscored: true});
    return groupMember;
};
