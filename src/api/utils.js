exports.auth_required = function(req, res, next) {
    if(req.isAuthenticated()){
        return next();
    }
    response = {
        message : "Unauthenticated request on path " + req.path
    }
    res.status(401).json(response);
};
