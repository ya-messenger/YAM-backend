const router = require('express').Router();
const path = require('path');
const multer = require('multer');
const uploadMiddleware = require('../middleware/upload');
const utils = require('./utils');

const storage = multer.diskStorage({
    destination: path.join(path.normalize(__dirname+'/../..'), 'uploads', 'profile'),
    filename: function(req, file, cb){
        let name = require('crypto')
                   .createHash('sha256')
                   .update(Date.now().toString())
                   .digest('hex')
        cb(null, name)
    }
});

const upload = multer({storage: storage});

router.post('/upload/profile', utils.auth_required, upload.single('avatar'), uploadMiddleware.uploadProfile);
router.post('/upload/group/:id', utils.auth_required, upload.single('group_avatar'), uploadMiddleware.uploadGroupAvatar);

module.exports = router;
