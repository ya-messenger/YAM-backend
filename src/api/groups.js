const router = require('express').Router();
const utils = require('./utils');
const groupMiddleware= require('../middleware/groups');

router.route('/groups')
    .get(utils.auth_required, groupMiddleware.getAuthUserGroups)
    .post(utils.auth_required, groupMiddleware.createNewGroup);

router.route('/groups/:group_id')
    .get(utils.auth_required, groupMiddleware.getGroupById);

module.exports = router;
