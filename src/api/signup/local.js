const router = require('express').Router();
const signup = require('../../middleware/signup/local');

router.post('/local', signup.localRegistration);

module.exports = router;
