const router = require('express').Router();
const utils = require('./utils');
const messageMiddleware = require('../middleware/messages');

router.route('/messages')
    .get(utils.auth_required, messageMiddleware.getMessages)
    .post(utils.auth_required, messageMiddleware.sendMessage);

router.route('/messages/:id')
    .get(utils.auth_required, messageMiddleware.getMessageById);

module.exports = router;
