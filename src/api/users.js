const router = require('express').Router();
const utils = require('./utils');
const userMiddleware = require('../middleware/users');

router.get('/users/:id', utils.auth_required, userMiddleware.getUserbyId);
router.get('/user', utils.auth_required, userMiddleware.getAuthenticatedUser);

module.exports = router;
