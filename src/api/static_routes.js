const router = require('express').Router();
const path = require('path');
const utils = require('./utils');
const env = require('../config/env');

router.get('/logout', function(req, res) {
    req.logout();
    res.sendStatus(200);
});

router.get('/login', function(req, res) {
    res.render('login');
});

router.get('/profile', utils.auth_required, function(req, res) {
    res.render('profile');
});

router.get('/upload', function(req, res) {
    res.render('upload');
});

router.get('/newgroup', utils.auth_required, function(req, res) {
    res.render('group');
});

router.get('/images/:id', function(req, res){
    res.sendFile(path.join(env.IMAGES_DIR, req.params.id));
});

module.exports = router;
