const router = require('express').Router();
const passport = require('../config/passport');

router.use('/auth', require('./auth/local'));
router.use('/signup', require('./signup/local'));

/*========== routes requiring sessions ==============*/
router.use(passport.initialize());
router.use(passport.session());
/*===================================================*/
router.use('/', require('./static_routes'));
router.use('/', require('./users'));
router.use('/', require('./messages'));
router.use('/', require('./upload'));
router.use('/', require('./groups'));

module.exports = router;
