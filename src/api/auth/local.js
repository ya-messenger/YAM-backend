const router = require('express').Router();
const passport = require('../../config/passport');
const auth = require('../../middleware/auth/local');

/*
 * Passport Session middleware
 */
router.use(passport.initialize());
router.use(passport.session());

router.route('/local')
    .get(auth.getLocalAuth)
    .post(
        passport.authenticate('local-auth'),
        auth.localAuth
        )

module.exports = router;
