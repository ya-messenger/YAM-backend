const sp = require('schemapack');

exports.userSchema = sp.build({
    username: 'string',
    jwt: 'string'
});
