const Socket = require('socket.io')

const io = Socket({
    origins: 'http://localhost:8080'
})

module.exports = io;
