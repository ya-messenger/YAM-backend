const app = require('./app')
const db = require('./models/db')
const http = require('http')
const io = require('./socket.io/socket-server')
const handleClient = require('./socket.io/client-handler')

const server = http.createServer(app.app)
io.attach(server)

/*
 * synchronize an initialize database
 */

db.sequelize.sync()
    .then(function() {
        console.log('Established connection to database...');
        server.listen(3000, function() {
            console.log('Server running at 3000...');
            console.log('CTRL+C to stop...');
        })
    })
    .catch(function(err) {
        console.log('Something went wrong with database update...');
    });

io.on('connection', handleClient);
