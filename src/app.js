const express = require('express');
const bodyParser = require('body-parser')
const ejs = require('ejs')
const morgan = require('morgan')
const session = require('express-session')
const redisStore = require('connect-redis')(session)
const cookieParser = require('cookie-parser')
const routes = require('./api/routes')
const db = require('./models/db')
const path = require('path')
const redisConfig = require('./config/database.json').redis

const app = express()

const CLIENT_URL = process.env.YAM_CLIENT_URL || 'http://localhost:8080'
var sessionStore = new redisStore(redisConfig)

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

app.use(morgan('common'))

app.use(session({
    secret: 'yam-secret',
    store: sessionStore,
    resave: false,
    saveUninitialized: true
}))

/*--- Allow CORS ---*/

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', CLIENT_URL);
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Expose-Headers', 'set-cookie');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

/*------------------*/

app.use(cookieParser())

app.use('/', routes);

app.set('views' , __dirname + '/views');
app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    res.render('signup.ejs')
})

module.exports = {app: app,
    sessionStore: sessionStore};
