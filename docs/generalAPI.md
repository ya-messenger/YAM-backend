# YAM-Backend API Documentation

## Signup

#### Basic Signup

**Endpoint**: `POST /signup/local`

**Parameters**:

|Name| Type | Description |
|----|------|-------------|
|user|string|unique username of new user`*`|
|pwd |string|password to be used for the user`*`|
|phone|string|phone number of the user`*`|
|email|string|email address of the new user`*`|

Fields marked with `*` are mandatory.

**Response**: 

If the username is available end everything is ok, a new User is created.
```
Status: 200 OK
```
```json
{
"id": 1,
"user": "yamdev"
}
```

If the username is not available.
```
Status: 403 Forbidden
```
```json
{
"message": "Username not available"
}
```

---

## Authentication

#### Basic Authentication

**Endpoint**: `POST /auth/local`

Parameters:

|Name|Type|Description|
|----|----|-----------|
|user|string|unique username`*`|
|pwd|string|password of the user`*`|

Fields marked with `*` are mandatory.

**Response**:

If the authentication is successful.
```
Status: 200 OK
```
```json
{
"id": 1,
"user": "yamdev"
}
```

If the authentication fails.
```
Status: 401 Unauthorized
```
```
Unauthorized
```

---

**Endpoint**: `GET /auth/local`

**Response**: 

Return data of authenticated user.
```
Status: 200 OK
```
```json
{
"id": 1,
"user": "yamdev"
}
```

else
```json
Status: 401 Unauthorized

{
"message" : "Unauthorized"
}
```
