# Groups API

Under construction.

## Get a signle groups data

**Endpoint**:  `GET /groups/:group_id`

Requires Authentication. 

Response with group detatils.

## Get groups of logged in user

**Endpoint**: `GET /groups`

Requires Authentication.

Responses with a list of objects.

## Create new group

**Endpoint**: `POST /groups`

Requires Authentication.
