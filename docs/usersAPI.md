# Users API

## Get single user details

**Endpoint**: `GET /users/:id`

Retrives the details of a single user. This endpoint required authentication.
If not authenticated a 403 Forbidden response will be send.

**Response**:

```
Status: 200 OK
```
```json
{
"id": 1,
"username": "yamdev",
"email": "yamdev@gmail.com",
"name": "Yam Dev",
"phone": "84XXXXXXXX",
"bio": "I am legend",
"avatar_url": "https://XXXXXX.XXXX"
"blood_type": "A+",
"role": "",
"updated_at": "2017-10-12T20:17:46.384Z",
"created_at": "2017-10-12T20:17:46.384Z"
}
```

---

## Get authenticated user details

**Endpoint**: `GET /user`

Retrives the details of the user authenticated. This endpoint also requires authentication.

**Response**:

```
Status: 200 OK
```
```
Similar data as /users/:id
```
